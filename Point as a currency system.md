﻿
# Point as a currency system

# Statement   
A point as a currency system. As a basic requirement, it will allow you to
assign certain points against a certain identity, create a virtual wallet, spend from the wallet, top-up or earn points for the wallet, transformation of points into actual currency based on certain rates in partner sites

![](flow.png)


## Requirements of the system 

1. Members can create a wallet
2. Members could view the balance of the wallet
3. Members could top-up the wallet and earn points
4. Members can use points on partners sites

## Assumptions 
- Members can top-up points by buying them
- Members can earn points by using offerings and services from:
    - **Wallet Owner Entity**: for example if the wallet owner entity is **AirAsia**, users can earn points by spending money on tickets or booking hotels offered by **AirAsia**
    - **Partners**: for example, if you redeem your points in a partner site to get an offering, or you buy from a partner site, you could earn points in return.

- Partners need to register their offerings in the system, this way, the system has control over what offerings the partner provide.

- The goal of the system is to make customers loyal, for example, customers will use/buy an offering, they will get points, these points will drive them to use/buy more offerings, this way, customers are loyal and constantly getting value, and generating value in return. 


## Main Actors of the system
1. **Member**: Utilize the wallet, view balance, earn and redeem points.
2. **Partner**: Provide Services/ Offerings that members can use by redeeming their points.
4. **Admin**: Mainly responsible for reviewing partners offerings before members are allowed to utilize them, it adds quality control for the offerings of the partners.

## Use cases

### User
1. **As a user**: I want to be able to view my points balance
2. **As a user**: I want to be able to top-up my points
3. **As a user**: I want to be able to redeem my points

### Partner 
1. **As a partner**: I want to be able to register my offerings that members can obtain them

### Admin 
1. **As an admin**: I want to be able to review, allow/ reject offerings provided by partners


![](use_case_diagram.png)

## Domain analysis 
- **Members** have **wallets**
- **Partners** have **offerings** 
- **Admins** can approve/ reject offerings 
- **Member** can redeem points to obtain a partners offering 

A simplified domain diagram looks like:
![](domain.PNG)

**Domain Events**:  

- Member Registered 
- Partner Posted Offering (for admin to approve)
- Admin Accepted Offering
- Admin Rejected Offering
- Member Requested Offering
- Member obtained offering 
- Balance Requested

![](event_storming.PNG)

## ERD
Shows the relationships of entity sets stored in a database

![](ERD.png)

## Activity diagram

The activity diagram for the main two flows, for viewing the balance and for redeeming points from partners site

![](activity.png)

## Services and modules 

### **Front End**: Angular/ React 

**Angular** is the most mature of the frameworks, has good backing in terms of contributors and is a complete package.

**React** is just old enough to be mature and has a huge number of contributions from the community. 

Both could do the job, react native could be used to develop the mobile apps.


### **Back-end Services**: Java Spring boot/ node.js

**Java** is one of the most mature frameworks out there to develop micro-services, using SpringBoot offers a lot of functionality out of the box, it takes out all the boilerplate code so the developer can focus on developing the actual application logic.  
  
**Node.js** is the go to framework for the web. Every time you run an IO call, Node JS doesn’t block the main-thread but submits the tasks to be executed by the internal IO daemon threads. Hence, micro-services Node JS gains popularity in terms of IO-bound tasks. 

1. **User Service** is responsible for adding, modifying or deleting users and all the aspects related to user management.
2. **Wallet Service** is responsible for creating wallets, top-up, redeem and delete wallets and all the aspects related to wallet transactions.
3. Notification Service is responsible for sending notifications (emails): 
    - Send an email when the user **creates a wallet**
    - Send an email when partners add **offerings** that might interest a user (targeted based on user history and profile)
    - When a partner wants to **register an offering**, a notification is sent to admins to review the service
    - a notification will be sent to partners when their offering's been **accepted or rejected**.

4. **Partner Service** is responsible for adding, deleting, modifying partners and also allow partners to register their offerings.

5. **Admin Service** is responsible for accepting or rejecting partners offerings and any admin related flows.

Services will be communicating through a **service bus** with each other.

Services could communicate through a single **gateway** with the outside world, this gateway could handle security as well, authentication and authorization.

**JWTs** **OAuth** could be used for securing the end points.


### **Messaging bus**: Apache Kafka, PubSub

In general, **Apache Kafka** and **PubSub** are very solid Stream processing systems. The point which makes the huge difference is that **Pubsub** is a **cloud service** attached to **GCP** whereas **Apache Kafka** can be used in both **Cloud and On-prem**

### **Database** 

**MySql**: is an open source and community is quite big, relational databases are a better fit for the wallet application because it's more stable and ensure data integrity. Since we are dealing with wallet transactions, we require ACID compliance (Atomicity, Consistency, Isolation, Durability) or defining exactly how transactions interact with a database. 

Bellow is a diagram of the proposed services: 

![](services.png)

## CI/CD

![](CI%20CD.png)

-  **Developers create Feature/ Hot fix branch** and deliver the expected outcome
- **Tests:** All features and code implemented should have tests, 3 type of tests need to be implemented:
    - **Unit tests** to cover the the business logic
    - Integration tests to cover how different service could work together 
    - End to End test
    
- **Pipeline:** the pipeline will **build**, **test** and **package** the different components, these components will be containerized

- **Code Reviews:** after the pipeline succeeds, the code needs to be reviewed by other developers

- **Dockerization**: All applications, fronted, back-end micro-services would be in docker containers. 
- **Orchestration: Kubernates**: Kubernates will be used for container orchestration to manage the deployment of these applications. 
   
![](kubernates_benifits.PNG)

- Environments:
    - **Dev** environment is local to the developer, normally the developer is working on a `feature` branch
    - **Integration**  environment deployment gets triggered when we merge to `master` branch
    - **Production** environment deployment gets triggered once we merge to `release` branch.
    
## Infrastructure  
- The **client** or the **member** connects to the front-end app through a **load-balancer** for better performance. If new nodes are added, the load balancer can equally distribute loads. 
- The partners connect through the **API Gateway** to the **Partner** service.

- For better security, only the **front-end** app and **API Gateway** are in **the public subnet** while the rest of the services and their database are in a private one.

- The **admin** can access the admin front-end app only through a **VPN**, this way, only internal employees can access it. 

- The micro-services are all contained in a **private subnet** which improves security.

- To deploy the code, we could push the containers to **GCP container registry**
- **Kubernates** cluster will manage deployments and scalability, it will pull images from the registry and deploy them as per the deployment script.
- **GCP Monitoring**  could be used to monitor the resources like CPU, RAM.

Bellow is a diagram of the proposed architecture:

![](infrastructure.PNG)

## Quality, Productivity and Agility 

To ensure quality of the software, agile practices need to be followed, a cycle or a sprint of 3-4 weeks allow for valuable deliverables and comprehensive testing and fast time to market.

New changes can be discussed in the beginning of the sprint, estimated delivery time would be discussed with developers and product owners/ 

Bugs need hot fixes, automated deployment processes is a key to ensure that 

In addition to that:
- Plan for a changeable environment, excepting change from the beginning will result in more extensible software.
- Monitor micro-services health and availability
- Perform automated sanity tests to ensure the system is functioning as expected. 
- Monitor resource usage, RAM, CPU, I/O and send alert when these resources usage exceeds a certain threshold.
- Perform load tests to the system before special deals to make sure that the system can handle huge amount of requests
- Practice clean code principle so that adding new features is very easy : [Clean Code Guidelines](https://www.linkedin.com/pulse/your-one-stop-clean-code-guidelines-article-sabir-moglad/)





